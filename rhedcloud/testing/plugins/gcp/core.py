"""
======================
Core GCP Test Fixtures
======================

This module contains the core fixtures used by GCP tests.

"""

from contextlib import ExitStack
from unittest import mock
import os

from googleapiclient import discovery
from google.oauth2.service_account import Credentials
import pytest

DEFAULT_API_KEY = os.getenv("DEFAULT_API_KEY", "/tmp/key_file")


pytest_plugins = [
    "rhedcloud.testing.plugins.generic.dict_fixture",
]


@pytest.fixture(scope="session", autouse=True)
def stack():
    """Create a stack that can be used to reliably roll back changes performed
    during testing.

    :yields:
        An ExitStack.

    """

    with ExitStack() as s:
        yield s


@pytest.fixture(scope="module", autouse=True)
def mod_stack():
    """Create a stack that can be used to reliably roll back changes performed
    during testing within a single module.

    :yields:
        An ExitStack.

    """

    with ExitStack() as s:
        yield s


@pytest.fixture(scope="session")
def project_id():
    """Retrieve the current GCP Project ID from the environment.

    :returns:
        A string or None.

    """

    return os.getenv("PROJECT_ID")


@pytest.fixture(scope="session")
def region():
    """Retrieve the GCP region for the current environment.

    :returns:
        A string or None.

    """

    return "us-east1"


@pytest.fixture(scope="module")
def sa_profile():
    """Return the name of the default Service Account profile to use.

    .. attention::

        This is intended to be overridden in specific test modules, as needed.

    """

    return None


@pytest.fixture(scope="module")
def sa_scopes():
    """Return the default scopes to use during testing.

    .. attention::

        This is intended to be overridden in specific test modules, as needed.

    :returns:
        A tuple of strings.

    """

    return (
        # "https://www.googleapis.com/auth/cloud-platform.read-only",
        "https://www.googleapis.com/auth/cloud-platform",
    )


@pytest.fixture(scope="module")
def unscoped_sa_creds(sa_profile, mod_stack):
    """Return unscoped Credentials for the default Service Account.

    :param str sa_profile:
        Name of the Service Account profile.
    :param ExitStack mod_stack:
        Reusable stack to help with rolling back changes.

    :returns:
        A Credentials object.

    """

    print("Using service account: {}".format(sa_profile or "DEFAULT"))
    path = os.getenv("GOOGLE_APPLICATION_CREDENTIALS")

    if sa_profile:
        path = DEFAULT_API_KEY
        if sa_profile:
            path = "{}_{}".format(path, sa_profile)

    print("Setting GOOGLE_APPLICATION_CREDENTIALS to: {}".format(path))
    mod_stack.enter_context(mock.patch.dict(os.environ, {
        "GOOGLE_APPLICATION_CREDENTIALS": path,
    }))

    return Credentials.from_service_account_file(path)


@pytest.fixture(scope="module")
def sa_creds(unscoped_sa_creds, sa_scopes):
    """Return scoped credentials for the default Service Account.

    :param unscoped_sa_creds:
        Unscoped Credentials.
    :param iter sa_scopes:
        An iterable of strings indicating which service scopes are required.

    :returns:
        A Credentials object.

    """

    return unscoped_sa_creds.with_scopes(sa_scopes)


@pytest.fixture(scope="module")
def api():
    """Return the core API that will be used for testing.

    Possible return values:

    * A 2-tuple containing the API name and version as two separate strings.

      ::

        return ("compute", "v1")

    * A single string containing the API name and version separated by a
      forward slash.

      ::

        return "compute/v1"

    * A single string containing only the API name.

      ::

        return "compute"

      In this scenario, the desired API version is assumed to be "v1".

    """

    raise NotImplementedError


@pytest.fixture(scope="module")
def api_client(sa_creds, api):
    """Build a default client for the core API that will be used for testing.

    :param sa_creds:
        Scoped Credentials.
    :param api:
        A 2-tuple or string indicating which API and version to build a client
        for.

    """

    if isinstance(api, str):
        api = api.split("/")

    if len(api) == 1:
        api = (api[0], "v1")

    return discovery.build(*api, credentials=sa_creds, cache_discovery=False)


pytest.unscoped_sa_creds = unscoped_sa_creds
pytest.build_api_client = api_client

# pytest.sa_scopes = lambda *s: pytest.mark.parametrize("sa_scopes", (s,), indirect=True)
