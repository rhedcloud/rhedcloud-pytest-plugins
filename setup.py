from setuptools import setup, find_packages

# AWS testing dependencies
AWS_REQUIRES = [
    "awscli",
    "boto3",
    "cfn-flip",
]

# GCP testing dependencies
GCP_REQUIRES = [
    "google-api-python-client",
    "google-cloud",
    "google-cloud-storage",
    "oauth2client",
]

DOCS_REQUIRES = [
    "sphinx",
    "sphinx-rtd-theme",
]


setup(
    name="rhedcloud-pytest-plugins",
    url="https://bitbucket.org/rhedcloud/rhedcloud-pytest-plugins",
    author="RHEDcloud",
    author_email="na@na.com",
    packages=find_packages(include=[
        "rhedcloud.*",
    ]),
    install_requires=[
        "docker",
        "jsonschema",
        "pytest",
        "pytest-mock",
        "pytest-xdist",
        "pyyaml",
        "redis",
    ],
    extras_require={
        "aws": AWS_REQUIRES,
        "docs": DOCS_REQUIRES,
        "gcp": GCP_REQUIRES,
        "all": AWS_REQUIRES + DOCS_REQUIRES + GCP_REQUIRES,
    },
)
