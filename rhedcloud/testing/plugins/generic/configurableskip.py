"""
==========================
Configurable Test Skipping
==========================

This is a ``pytest`` plugin that allows certain test to be skipped dynamically
based on the configuration in ``pytest.ini``. Skip tests by editing the
``skip_tests`` list:

.. code-block:: ini

    [pytest]
    skip_tests =
        test_config.py
        *batch*

"""

from collections import namedtuple
import fnmatch
import itertools
import os

import pytest


TestPattern = namedtuple('TestPattern', ["pattern", "comment"])


def pytest_addoption(parser):
    parser.addini(
        'skip_tests',
        'Tests to skip. Supports globbing.',
        type='linelist',
    )


@pytest.hookimpl(tryfirst=True)
def pytest_collection_modifyitems(config, items):
    """Dynamically add a "skip" mark to certain tests that are listed in
    pytest.ini.

    """

    skip = pytest.mark.skip

    patterns = set(load_patterns(config, "skip_tests"))
    for item in items:
        matches, pattern = item_matches(item, patterns)
        if matches:
            item.add_marker(skip(reason=pattern.comment))


def load_patterns(config, key):
    """Load test patterns from pytest's configuration.

    Test patterns may include a comment to describe why a test pattern is in
    place by using a # symbol after the pattern. For example::

        skip_tests =
            test_sample.py  # this is a good reason to skip these tests

    :yields:
        TestPattern objects containing each pattern and the associated comment.

    """

    patterns = config.getini(key)
    for pattern in patterns:
        pattern, _, comment = pattern.partition("#")
        if not comment:
            comment = "See pytest.ini (pattern: {})".format(pattern)

        yield TestPattern(pattern.strip(), comment.strip())


def get_item_identifiers(item):
    """Generate a set of identifiers for a pytest test function.

    :param _pytest.nodes.Item item:
        A pytest test function.

    :returns:
        A set of strings.

    """

    nodeid = item.nodeid

    identifiers = {
        item.name,
        remove_params(item.name),
        nodeid,
        remove_params(nodeid),
        os.path.basename(str(item.fspath)),
    }

    if "/" in nodeid:
        mod_and_func = nodeid[nodeid.rindex("/") + 1:]
        identifiers.add(mod_and_func)
        identifiers.add(remove_params(mod_and_func))

    if "" in identifiers:
        identifiers.remove("")

    return identifiers


def item_matches(item, patterns):
    """Determine whether the specified item matches any of the specified patterns.

    :param _pytest.nodes.Item item:
        A pytest test function.
    :param set(str) patterns:
        A set of patterns to compare with the test function's ID.

    :returns:
        A 2-tuple containing:

        - a boolean, which indicates whether the item matched a pattern
        - a string, which would be the pattern that the item matched (or None
          if the item did not match)

    """

    identifiers = get_item_identifiers(item)

    for pattern, identity in itertools.product(patterns, identifiers):
        if fnmatch.fnmatch(identity, pattern.pattern):
            return True, pattern

    return False, None


def remove_params(nodeid):
    """Strip out the values for parametrized tests.

    :param str nodeid:
        A pytest test function's node ID.

    :returns:
        A string.

    >>> remove_params("tests/functional/test_service.py[vpc-1234-OrgUnit]")
    'tests/functional/test_service.py'
    >>> remove_params("tests/functional/test_service.py")
    'tests/functional/test_service.py'

    """

    if "[" in nodeid and nodeid.endswith("]"):
        nodeid = nodeid[:nodeid.index("[")]

    return nodeid
