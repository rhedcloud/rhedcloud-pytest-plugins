"""
===================
AWS Account Locking
===================

This is a ``pytest`` plugin to support exclusive access to an AWS Account for
the duration of the ``pytest`` session.

"""

import pytest

from aws_test_functions.orgs import ACCOUNT_NAME

pytest_plugins = [
    "rhedcloud.testing.plugins.generic.account_locking",
]


@pytest.fixture(scope="session")
def account_locking_key():
    return ACCOUNT_NAME
