"""
=================
AWS Script Runner
=================

This is a pytest plugin to facilitate the execution of commands within an AWS
VPC using SSM.

Configuration
=============

Changing VPC Type
-----------------

:envvar:`RHEDCLOUD_VPC_TYPE` may used to specify the type of VPC being tested
in the current pytest session. Defaults to ``'1'``.

Alternatively, this value may also be configured using the
``rhedcloud_vpc_type`` option in ``pytest.ini``.

Changing "Internal" Subnet Name
-------------------------------

:envvar:`RHEDCLOUD_INTERNAL_SUBNET` may used to specify the name of subnet
within the customer VPC that will be used to create the "internal" script
runner instance. Defaults to ``'Public Subnet 1'``.

Alternatively, this value may also be configured using the
``rhedcloud_internal_subnet`` option in ``pytest.ini``.

"""

from functools import partial
import os
import time

import boto3
import pytest

from aws_test_functions import (
    aws_client,
    build_command_runner,
    create_ec2_instance,
    create_instance_profile,
    create_session_for_test_user,
    create_ssm_role,
    delete_ec2_instance,
    env,
    find_ec2_instance,
    get_subnet_id,
    get_vpc_ids,
    setup_ssm_service_endpoints,
)

# The ID of an AMI to use when creating the script runners. This should point
# to a custom AMI that includes common utilities required by the tests.
AMI_ID = os.getenv("SCRIPT_RUNNER_AMI") or 'ami-0e30195e44510f2f8'


# this keeps track of when the session-wide script runner Instance Profile was
# created, so we know if we need to wait for it to be ready before attempting
# to attach it to an EC2 instance.
_INSTANCE_PROFILE_CREATED = -1


VPC_IDS = get_vpc_ids(env.RHEDCLOUD_VPC_TYPE)


pytest_plugins = [
    'rhedcloud.testing.plugins.aws.admin_user',
]


def pytest_addoption(parser):
    # allow the VPC type to be configured in pytest.ini
    parser.addini(
        'rhedcloud_vpc_type',
        'Type of VPC to use for testing',
        default=env.RHEDCLOUD_VPC_TYPE,
    )

    # allow the subnet name to be configured in pytest.ini
    parser.addini(
        'rhedcloud_internal_subnet',
        'Name of the "internal" subnet in the specified VPC type',
        default=env.RHEDCLOUD_INTERNAL_SUBNET,
    )


@pytest.mark.tryfirst
def pytest_configure(config):
    """Load configuration values from pytest.ini"""

    env.RHEDCLOUD_VPC_TYPE = config.getini('rhedcloud_vpc_type')
    env.RHEDCLOUD_INTERNAL_SUBNET = config.getini('rhedcloud_internal_subnet')


@pytest.fixture(scope='session')
def _ssm(admin_session):
    """Create a new SSM client.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User with permission to
        interact with SSM APIs.

    :returns:
        A handle to the SSM API.

    """

    return admin_session.client('ssm')


@pytest.fixture(scope='session')
def global_session(user):
    """Create a default session for the test user that is scoped to the pytest session."""

    return create_session_for_test_user(user.user_name)


@pytest.fixture(scope='session')
def ec2res(global_session):
    return global_session.resource('ec2')


@pytest.fixture(scope='session', params=VPC_IDS)
def vpc_id(request):
    """ID of an RHEDcloud VPC.

    This fixture may change if there are multiple VPCs of the same type in the
    AWS account.

    :param pytest.request request:
        A pytest request object.

    :returns:
        A string.

    """

    try:
        vid = request.param
        print('Using VPC (type {}): {}'.format(env.RHEDCLOUD_VPC_TYPE, vid))
    except (TypeError, AttributeError):
        raise ValueError('No VPCs of type {} found in the current AWS account'.format(env.RHEDCLOUD_VPC_TYPE))

    return vid


@aws_client("ec2")
def get_internal_subnet_id(vpc_id, *, ec2):
    """ID of a subnet within the RHEDcloud VPC.

    :param str vpc_id:
        ID of an RHEDcloud VPC.
    :param EC2.Client ec2:
        Handle to the EC2 API.

    :returns:
        A string.

    """

    return get_subnet_id(vpc_id, env.RHEDCLOUD_INTERNAL_SUBNET, ec2=ec2)


@pytest.fixture(scope='session')
def internal_subnet_id(vpc_id, ec2res):
    """ID of a subnet within the RHEDcloud VPC.

    :param str vpc_id:
        ID of an RHEDcloud VPC.
    :param EC2.ServiceResource ec2res:
        Handle to the EC2 Service Resource API.

    :returns:
        A string.

    """

    return get_internal_subnet_id(vpc_id, ec2=ec2res.meta.client)


@pytest.fixture(scope='session')
def ssm_service_endpoints(vpc_id):
    """Setup the correct AWS Endpoints to be able to use SSM.

    :param str vpc_id:
        ID of an RHEDcloud VPC.

    """

    setup_ssm_service_endpoints(vpc_id)


def get_script_runner_role(ssm_service_endpoints):
    """Create or retrieve an IAM Role that is suitable for use with SSM.

    This role is not automatically removed, in an effort to speed up subsequent
    executions of pytest.

    :param ssm_service_endpoints:
        A dependency on the fixture that ensures the SSM Endpoints are created
        in the RHEDcloud VPC.

    :returns:
        An IAM Role resource.

    """

    name = 'test_using_ssm'
    iam = boto3.resource('iam')

    try:
        role = iam.Role(name)
        assert role.role_id
        print('Found existing SSM Role')
    except Exception:
        role = create_ssm_role(name)

    return role


@pytest.fixture(scope='session')
def script_runner_role(ssm_service_endpoints):
    """Create or retrieve an IAM Role that is suitable for use with SSM.

    This role is not automatically removed, in an effort to speed up subsequent
    executions of pytest.

    :param ssm_service_endpoints:
        A dependency on the fixture that ensures the SSM Endpoints are created
        in the RHEDcloud VPC.

    :returns:
        An IAM Role resource.

    """

    return get_script_runner_role(ssm_service_endpoints)


def get_script_runner_instance_profile(script_runner_role):
    """Get or create a reusable Instance Profile to attach to EC2 instances so
    they can be managed with SSM.

    :param IAM.Role script_runner_role:
        An IAM Role resource that has been setup for SSM.

    :returns:
        An IAM Instance Profile resource.

    """

    global _INSTANCE_PROFILE_CREATED

    name = 'script_runner'
    iam = boto3.resource('iam')

    try:
        instance_profile = iam.InstanceProfile(name)
        assert instance_profile.arn
        print('Found existing SSM Instance Profile')
    except Exception:
        # create a new Instance Profile using the new role. This uses
        # `wait=False` on the assumption that it will get created before a call
        # that takes a while (like setting up an RDS instance or an ELB).
        instance_profile = create_instance_profile(name, role=script_runner_role, wait=False)
        _INSTANCE_PROFILE_CREATED = time.time()

    return instance_profile


@pytest.fixture(scope='session', autouse=True)
def script_runner_instance_profile(script_runner_role):
    """Get or create a reusable Instance Profile to attach to EC2 instances so
    they can be managed with SSM.

    :param IAM.Role script_runner_role:
        An IAM Role resource that has been setup for SSM.

    :returns:
        An IAM Instance Profile resource.

    """

    return get_script_runner_instance_profile(script_runner_role)


def _wait_for_instance_profile(seconds=30):
    """Make sure the Instance Profile has had sufficient time to get ready.

    :param int seconds:
        Number of seconds required for the Instance Profile to have enough time
        to get setup and be usable.

    """

    global _INSTANCE_PROFILE_CREATED

    # bail out if we're using an existing instance profile
    if _INSTANCE_PROFILE_CREATED < 0:
        return

    time_since_profile_creation = time.time() - _INSTANCE_PROFILE_CREATED
    if time_since_profile_creation < seconds:
        print('Waiting for Instance Profile to be ready...')
        time.sleep(seconds - time_since_profile_creation)


def do_setup_runners(script_runner_instance_profile, internal_subnet_id, _ssm=None):
    """Create an internal and external EC2 instance to run script in different
    contexts.

    :param IAM.InstanceProfile script_runner_instance_profile:
        An IAM Instance Profile resource that is setup for SSM.
    :param str internal_subnet_id:
        The ID of a subnet in the RHEDcloud VPC.

    :returns:
        A dictionary.

    """

    # setup internal script runner (in RHEDcloud VPC)
    internal = create_script_runner(
        'internal_runner',
        script_runner_instance_profile,
        SubnetId=internal_subnet_id,
        ssm=_ssm,
    )

    # setup external script runner (in default VPC)
    external = create_script_runner(
        'external_runner',
        script_runner_instance_profile,
        ssm=_ssm,
    )

    return {
        'internal': internal,
        'external': external,
    }


@pytest.fixture(scope='session')
def setup_runners(script_runner_instance_profile, internal_subnet_id, _ssm=None):
    """Create an internal and external EC2 instance to run script in different
    contexts.

    :param IAM.InstanceProfile script_runner_instance_profile:
        An IAM Instance Profile resource that is setup for SSM.
    :param str internal_subnet_id:
        The ID of a subnet in the RHEDcloud VPC.

    :returns:
        A dictionary.

    """

    return do_setup_runners(script_runner_instance_profile, internal_subnet_id, _ssm)


@pytest.fixture(scope='session')
def new_script_runner(script_runner_instance_profile, _ssm):
    """Fixture to help create an EC2 instance and that can be used to run
    scripts using SSM and easily retrieve the command output.

    :param IAM.InstanceProfile script_runner_instance_profile:
        An IAM Instance Profile resource that is setup for SSM.
    :param SSM.Client _ssm:
        Client with permission to interact with SSM APIs.

    :returns:
        A function that expects an EC2 instance name and any keyword arguments
        necessary to configure the EC2 instance.

    """

    return partial(create_script_runner, profile=script_runner_instance_profile, ssm=_ssm)


@aws_client('ssm')
def create_script_runner(name, profile, ssm=None, **kwargs):
    """Get or create an EC2 instance that can be used to execute scripts using
    SSM and easily retrieve the command output.

    :param str name:
        The name of the EC2 Instance to get or create.
    :param IAM.InstanceProfile profile:
        An IAM Instance Profile resource that is setup for SSM.
    :param SSM.Client ssm:
        SSM Client.

    :returns:
        A function.

    """

    defaults = dict(
        size='t2.small',
        ami=AMI_ID,
    )

    try:
        subnet_id = kwargs.get('SubnetId')
        inst = find_ec2_instance(name, subnet_id=subnet_id)
        print("Instance ID: " + inst.instance_id)

        print('Instance subnet: {} (want {})'.format(inst.subnet_id, subnet_id or '<default>'))

        print("Instance AMI ID: {} (want {})".format(inst.image_id, AMI_ID))
        if inst.image_id != AMI_ID:
            print("New AMI_ID Detected")
            delete_ec2_instance(inst, **kwargs)
            raise ValueError('New AMI_ID Detected')

        print('Found existing EC2 instance: {}'.format(name))
    except ValueError:
        # set the instance profile for the EC2 instance that will run the script
        kwargs['profile'] = profile

        # supply default values without overwriting any custom parameters
        for key, value in defaults.items():
            if key not in kwargs:
                kwargs[key] = value

        # make sure the instance profile has had enough time to settle before
        # trying to attach it to an EC2 instance
        _wait_for_instance_profile()

        inst = create_ec2_instance(name, **kwargs)
        inst.wait_until_running()

    return build_command_runner(inst, ssm=ssm)


@pytest.fixture(scope='session')
def external_runner(setup_runners):
    """Return the runner in the default VPC.

    :param dict setup_runners:
        A dictionary containing different script runners.

    :returns:
        A function.

    """

    return setup_runners['external']


@pytest.fixture(scope='session')
def internal_runner(setup_runners):
    """Return the runner in the RHEDcloud VPC.

    :param dict setup_runners:
        A dictionary containing different script runners.

    :returns:
        A function.

    """

    return setup_runners['internal']
