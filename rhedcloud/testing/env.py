"""
============================
Custom Environment Variables
============================

This module contains custom environment variables to alter the behavior of
helper functions.

"""

import os


# Redis connection information
REDIS_HOST = os.getenv("REDIS_HOST")
REDIS_PORT = os.getenv("REDIS_PORT")
REDIS_DB = int(os.getenv("REDIS_DB", 0))
REDIS_PASSWORD = os.getenv("REDIS_PASSWORD")
