"""
==============
Test ID Plugin
==============

This is a ``pytest`` plugin that looks at the docstrings for each test function
to detect which test IDs are supposed to be addressed by each test.

"""

from functools import partial
import re

import pytest


RE_TESTCOUNT = re.compile(r'\$\{testcount:(\d+)\}')
RE_TESTID = re.compile(r'(\w+\-[fs]\-\d+\-\d+)')


@pytest.hookimpl(tryfirst=True)
def pytest_itemcollected(item):
    """Detect any test IDs in a test's docstring"""

    item._test_ids = find_test_ids(item.function.__doc__)
    item.extra_keyword_matches.update(item._test_ids)

    if item._test_ids and item.config.getoption('verbose'):
        item._nodeid = '[{}] {}'.format(', '.join(item._test_ids), item.nodeid)


@pytest.hookimpl(trylast=True)
def pytest_collection_modifyitems(config, items):
    """Generate a quick count of tests that are detected in each module.

    Report layout::

        {
            "path/module.py": {
                "testcount": "${testcount:X}",
                "detected": [
                    "mod-f-1-1",
                    "mod-f-1-2",
                    "mod-f-1-n"
                ]
            }
        }

    """

    modules = {}
    for item in items:
        mod = item.location[0]
        if mod not in modules:
            test_count = RE_TESTCOUNT.search(item.module.__doc__ or '')
            modules[mod] = {
                'testcount': int(test_count.group(1)) if test_count else 0,
                'detected': [],
            }

        modules[mod]['detected'].extend(item._test_ids)

    for mod, spec in modules.items():
        spec['detected'] = sort_test_ids(spec['detected'])

    config._test_ids = modules


def sorter(value):
    """Use numeric sorting for test IDs.

    :param str value:
        A test ID, such as abc-f-1-1.

    :returns:
        A list of values to use when sorting test IDs.

    Usage:

    >>> test_ids = ['abc-f-1-2', 'abc-f-1-1', 'abc-f-1-10']
    >>> sorted(test_ids)
    ['abc-f-1-1', 'abc-f-1-10', 'abc-f-1-2']
    >>> sorted(test_ids, key=sorter)
    ['abc-f-1-1', 'abc-f-1-2', 'abc-f-1-10']

    """

    bits = value.split('-')

    try:
        bits[-2] = int(bits[-2])
        bits[-1] = int(bits[-1])
    except ValueError:
        pass

    return bits


sort_test_ids = partial(sorted, key=sorter)


def find_test_ids(string):
    """Find all test IDs that appear near ``:testid:`` in a test function's
    docstring.

    :param str string:
        A string that is expected to contain one or more test IDs.

    :returns:
        A list of strings.

    """

    string = string or ''

    test_ids = []
    scanning, searching = True, False
    for line in string.splitlines():
        if scanning and ':testid:' in line:
            scanning, searching = False, True

        if searching:
            test_ids.extend([i.strip() for i in RE_TESTID.findall(line)])
            if not test_ids:
                break

    return test_ids


def pytest_terminal_summary(terminalreporter):
    """Display a report of test IDs that were detected in each of the tests."""

    tr = terminalreporter
    tr.write_sep("=", "test id report")

    for module, details in tr.config._test_ids.items():
        if not details['detected']:
            continue

        test_ids = sort_test_ids(set(details['detected']))
        tr.write_line("{}: {}".format(module, ', '.join(test_ids)))
