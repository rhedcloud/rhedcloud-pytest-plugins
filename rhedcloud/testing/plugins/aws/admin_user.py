"""
==============
AWS Admin User
==============

This is a pytest plugin that manages a user with administrative privileges,
which allows access to services that are controlled via IAM policies (rather
than Service Control Policies).

"""

import pytest

from aws_test_functions import create_session_for_test_user, new_user


@pytest.fixture(scope='session')
def admin_user(in_setup_org, session_stack):
    """Create a user specifically to issue privileged commands."""

    with new_user('admin-user', 'AdministratorAccess', stack=session_stack) as u:
        yield u


@pytest.fixture(scope='session')
def admin_session(admin_user):
    """Create a new SSM client.

    :param IAM.User admin_user:
        An IAM User resource for a user with permission to interact with
        services that are controlled via IAM policies.

    :returns:
        A handle to the SSM API.

    """

    return create_session_for_test_user(admin_user.user_name)
