.. RHEDcloud Shared Test Utilities documentation master file, created by
   sphinx-quickstart on Thu Dec  7 23:58:51 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=========================================================
Welcome to RHEDcloud Shared Test Utilities documentation!
=========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   env
   redis
   utils

Generic Pytest Plugins
======================

The following are generic, provider-agnostic pytest plugins.

.. toctree::
   :maxdepth: 2
   :caption: Generic Plugins:

   plugins/generic/account_locking
   plugins/generic/configurableskip
   plugins/generic/dict_fixture
   plugins/generic/documented_tests

Pytest Plugins for AWS
======================

The following are pytest plugins for AWS.

.. toctree::
   :maxdepth: 2
   :caption: AWS Plugins:

   plugins/aws/accessdenied
   plugins/aws/account_locking
   plugins/aws/admin_user
   plugins/aws/blocked_services
   plugins/aws/scp_tester
   plugins/aws/script_runner

Pytest Plugins for GCP
======================

The following are pytest plugins for GCP.

.. toctree::
   :maxdepth: 2
   :caption: GCP Plugins:

   plugins/gcp/core

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
