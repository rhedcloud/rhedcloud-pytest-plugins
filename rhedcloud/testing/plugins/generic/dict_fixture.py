"""
=========================
Dictionary Fixture Plugin
=========================

This is a ``pytest`` plugin that makes it easy to create multiple fixtures from
the keys in a dictionary. This provides a means for sharing values between
multiple test functions with relative ease.

"""

import pytest

from rhedcloud.testing.utils import build_fixture


def dict_fixture(orig_fn=None, *, scope='module', autouse=True,
                 with_assertion=True, **kwargs):
    """Dynamically generate a series of pytest fixtures from the keys in the
    dictionary returned by the decorated function.

    Usage:

    .. code-block:: python

        import pytest


        @pytest.dict_fixture
        def shared_vars():
            return {
                'fixture_a': None,
                'fixture_b': None,
                'fixture_c': None,
            }


        def test_fixture_a(fixture_a):
            assert fixture_a is None


        def test_update_fixture_a(shared_vars):
            shared_vars['fixture_a'] = 'testing'


        def test_fixture_a_again(fixture_a):
            assert fixture_a == 'testing'

    The code snippet above will result in the following pytest fixtures being
    created and made available within the same module:

    * ``shared_vars``: a module-scoped fixture
    * ``fixture_a``: a function-scoped fixture
    * ``fixture_b``: a function-scoped fixture
    * ``fixture_c``: a function-scoped fixture

    The values returned by the three dynamically-generated fixtures will reflect
    the values set in the ``shared_vars`` fixture dictionary.

    """

    kwargs.update({
        'scope': scope,
        'autouse': autouse,
    })

    def _decorate(fn, depth=2):
        template = fn()
        if not isinstance(template, dict):
            raise TypeError('pytest.dict_fixture functions must return a dictionary')

        for key in template:
            build_fixture(key, with_assertion=with_assertion, depth=depth)

        return pytest.fixture(**kwargs)(fn)

    if orig_fn:
        return _decorate(orig_fn, 3)

    return _decorate


pytest.dict_fixture = dict_fixture
