"""
========================
AWS Access Denied Plugin
========================

This is a ``pytest`` plugin that wraps any test function marked with
``raises_access_denied`` with ``pytest.raises`` to ensure the test only passes
when an Access Denied error is raised by ``boto3``.

For example:

.. code-block:: python

    @pytest.mark.raises_access_denied
    def test_restricted_call(iam):
        iam.delete_user(UserName='this is fake')

is equivalent to:

.. code-block:: python

    def test_restricted_call(iam):
        with pytest.raises(ClientError, match='AccessDenied'):
            iam.delete_user(UserName='this is fake')

Bypassing ``@pytest.mark.raises_access_denied``
===============================================

For tests that are marked with ``@pytest.mark.raises_access_denied``
dynamically, sometimes those tests may do nothing that actually triggers a
restricted API. This can cause unexpected errors when the test does not
encounter an "Access Denied" error. To circumvent this type of situation, such
tests may be marked with ``@pytest.mark.bypass('raises_access_denied')``. This
will remove the expectation of an "Access Denied" error for the function.

Configuration-Based Bypassing
-----------------------------

Certain hard-coded ``@pytest.mark.raises`` or
``@pytest.mark.raises_access_denied`` marks may be bypassed by adding some
lines to the ``pytest.ini`` file:

.. code-block:: ini

    bypass_raises_access_denied =
        test_iam_groups.py
        test_iam_users.py
        test_vpc.py

    bypass_raises =
        test_vpc.py::test_modify_subnet
        test_vpc.py::test_create_kinesis_streams_endpoint

Overriding Exception Patterns
=============================

By default, the ``@pytest.mark.raises_access_denied`` marker looks for error
messages that include one of the following keywords:

* ``AccessDenied``
* ``Unauthorized``
* ``Forbidden``

These patterns may be overridden on a per-module or per-test basis if
necessary.

Override Per Module
-------------------

To override the pattern used to detect "Access Denied" errors for all tests in
a given module, set the ``ACCESS_DENIED_NEEDLE`` global variable:

.. code-block:: python

    ACCESS_DENIED_NEEDLE = 'CustomException'

With such an override, the ``@pytest.mark.raises_access_denied`` marker will
allow tests in that module to pass only if they raise an exception containing
``CustomException`` in the error message.

Alternatively, you may specify a regular expression:

.. code-block:: python

    ACCESS_DENIED_NEEDLE = r'(Custom|Special)Error'

Override Per Test
-----------------

If a specific test function raises an exception that should be considered an
"Access Denied" error but does not contain the same pattern as everything else,
the ``@pytest.mark.raises_access_denied_needle`` marker may be used to override
the pattern for individual test functions.

.. code-block:: python

    @pytest.mark.raises_access_denied_needle(r'(Custom|Special)Error')
    def test_usual_exceptions():
        ...

Conditional Exceptions
----------------------

It's possible to indicate that a test is expected to raise a certain exception
based on parameters passed to the test. For example:

.. code-block:: python

    def var_is_two(pyfuncitem):
        return pyfuncitem.funcargs["var"] == 2


    @pytest.mark.raises(ZeroDivisionError, when=var_is_two)
    @pytest.mark.parametrize("var", (1, 2, 3))
    def test_the_var(var):
        if var == 2:
            var = 0
        assert 10 / var

The test above would execute three times; once with ``var = 1``, again with
``var = 2``, and a third time with ``var = 3``. The ``raises`` mark is passed a
``when`` parameter, which is expected to be a callable that accepts a single
Parameter, which is the test function that pytest is about to execute. Each of
the executions of the test function will pass, even though the second execution
Would normally raise a ``ZeroDivisionError``.

"""

from contextlib import ExitStack

from botocore.exceptions import ClientError
import pytest

from rhedcloud.testing.plugins.generic.configurableskip import load_patterns, item_matches


def pytest_configure(config):
    config.addinivalue_line(
        'markers',
        'raises_access_denied: expect the test function to raise an "Access Denied" exception')

    config.addinivalue_line(
        'markers',
        'raises(expected_exception=Exception, *args, **kwargs): wraps the test function in a "with pytest.raises(expected_exception=Exception, *args, **kwargs)" context manager')


def pytest_addoption(parser):
    for mark in ("raises", "raises_access_denied"):
        parser.addini(
            'bypass_{}'.format(mark),
            'Bypass the "{}" mark for certain tests.'.format(mark),
            type='linelist',
        )


@pytest.hookimpl(trylast=True)
def pytest_collection_modifyitems(config, items):
    """Dynamically bypass certain "raises" or "raises_access_denied" marks
    based on values in pytest.ini.

    """

    for mark in ("raises", "raises_access_denied"):
        patterns = set(load_patterns(config, "bypass_{}".format(mark)))
        for item in items:
            matches, pattern = item_matches(item, patterns)
            if matches:
                item.add_marker(pytest.mark.bypass(mark, reason=pattern.comment))


@pytest.hookimpl(hookwrapper=True)
def pytest_pyfunc_call(pyfuncitem):
    stack = ExitStack()
    args = []
    kwargs = {}

    # iterate over all markers for a specific test function
    for mark in pyfuncitem.iter_markers():
        # determine whether the marker has a handler here
        handler = _MARK_HANDLERS.get(mark.name)
        if not callable(handler):
            continue

        # determine whether the marker is worth processing further
        args, kwargs = handler(mark, pyfuncitem)
        if args or kwargs:
            predicate = kwargs.get("when")
            kwargs = {k: v for k, v in kwargs.items() if k != "when"}

            # determine whether conditions are such that an exception is expected
            if (
                not callable(predicate) or
                (callable(predicate) and predicate(pyfuncitem))
            ):
                stack.enter_context(pytest.raises(*args, **kwargs))

    with stack:
        outcome = yield
        outcome.get_result()

    # if we get here, it means the expected exception was indeed raised, so
    # the test should be marked as passing
    outcome.force_result(True)


def _handle_raises(mark, pyfuncitem):
    """Retrieve the positional and keyword arguments for each
    ``@pytest.mark.raises`` marker that does not also have an associated
    ``bypass`` marker.

    :param _pytest.mark.structures.Mark mark:
        An object that represents a pytest marker.
    :param pytest.Function pyfuncitem:
        The internal representation of a test function.

    :returns:
        A 2-tuple containing the positional and keyword arguments for the
        marker, if any.

    """

    args, kwargs = [], {}
    if mark and not bypass(pyfuncitem, 'raises'):
        args = mark.args
        kwargs.update(mark.kwargs)

    return args, kwargs


def _handle_raises_access_denied(mark, pyfuncitem):
    """Retrieve the positional and keyword arguments for each
    ``@pytest.mark.raises_access_denied`` marker that does not also have an
    associated ``bypass`` marker.

    :param _pytest.mark.structures.Mark mark:
        An object that represents a pytest marker.
    :param pytest.Function pyfuncitem:
        The internal representation of a test function.

    :returns:
        A 2-tuple containing the positional and keyword arguments for the
        marker, if any.

    """

    args, kwargs = [], {}
    if mark and not bypass(pyfuncitem, 'raises_access_denied'):
        args = (ClientError,)
        kwargs.update(mark.kwargs)

        # allow a needle to be set as a kwarg of the marker
        needle = kwargs.pop('needle', None)
        if not needle:
            # if not supplied as a kwarg, fall back to module-level needles or
            # the default needle
            needle = getattr(
                pyfuncitem.module,
                'ACCESS_DENIED_NEEDLE',
                r'.*(AccessDenied|Unauthorized|Forbidden).*',
            )

        # allow individual functions to override the needle (useful for needle reuse)
        override = pyfuncitem.get_closest_marker('raises_access_denied_needle')
        if override and override.args:
            needle = override.args[0]

        kwargs["match"] = needle

    return args, kwargs


def bypass(item, needle):
    """Determine whether or not to bypass the specified marker.

    :param item:
        The internal representation of a test function.
    :param str needle:
        The name of a "raises" mark to bypass.

    :returns:
        A boolean.

    """

    config = item.get_closest_marker('bypass')
    return config and needle in config.args


_MARK_HANDLERS = {
    "raises": _handle_raises,
    "raises_access_denied": _handle_raises_access_denied,
}
