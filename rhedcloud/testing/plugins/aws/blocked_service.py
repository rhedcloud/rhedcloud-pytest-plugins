"""
===========================
AWS Blocked Services Plugin
===========================

This is a ``pytest`` plugin for AWS services that are blocked by RHEDcloud
Service Control Policies. When an AWS service is blocked by a Service Control
Policy, the tests are first executed within the ``SETUP_ORG``, on the
assumption that they are simple smoke tests and will all pass.

The tests are also executed within the ``TEST_ORG``, just like the rest of the
tests. However, under these circumstances, the tests are will only pass when an
AccessDenied error is raised by ``boto3``.

To mark an AWS service as blocked, edit the ``pytest.ini`` file, adding it to
the ``rhedcloud_blocked_services`` list:

.. code-block:: ini

    [pytest]
    rhedcloud_blocked_services =
        config
        batch

"""

from collections.abc import Iterable
import inspect
import time

import pytest

from aws_test_functions.lib import ClientError
from aws_test_functions.orgs import SETUP_ORG, TEST_ORG

BLOCKED_SERVICES = set()
SCP_REQD_SERVICES = set()
HIPAA_PIPELINE = False


def pytest_addoption(parser):
    parser.addini(
        'rhedcloud_blocked_services',
        'AWS Services blocked by RHEDcloud Service Control Policies',
        type='linelist',
    )

    parser.addini(
        'rhedcloud_scp_required_services',
        'AWS Services which must be tested using an SCP only (implies being a blocked service)',
        type='linelist',
    )

    parser.addini(
        'rhedcloud_hipaa_pipeline',
        'Whether the current pipeline is for testing HIPAA compliance',
        type='bool',
        default=False,
    )


@pytest.hookimpl(tryfirst=True)
def pytest_configure(config):
    """Determine which services are blocked by RHEDcloud"""

    global BLOCKED_SERVICES, SCP_REQD_SERVICES, HIPAA_PIPELINE

    BLOCKED_SERVICES = set(config.getini('rhedcloud_blocked_services'))
    SCP_REQD_SERVICES = set(config.getini('rhedcloud_scp_required_services'))
    HIPAA_PIPELINE = bool(config.getini("rhedcloud_hipaa_pipeline"))

    config.addinivalue_line(
        "markers",
        "test_with_and_without_scp: test both with and without SCP in effect, without expecting access denied errors when the SCP is in effect"
    )

    config.addinivalue_line(
        "markers",
        "rhedcloudsnapshotshareblock",
    )


def pytest_generate_tests(metafunc):
    """Change the behavior of pytest to automatically run tests for blocked
    services twice:

    * once in the setup org, where they should pass
    * once in the test org, where they should fail because the service control
      policy should restrict access to the API for the blocked service

    When a service is not blocked, the tests will only be executed once (in the
    test org).

    """

    # assume all tests will run in the test org
    org_units = [TEST_ORG]
    mod = metafunc.module

    if not HIPAA_PIPELINE and "hipaa" in mod.__name__:
        metafunc.parametrize("in_correct_org", [
            pytest.param(TEST_ORG, marks=[pytest.mark.skip("Not a HIPAA pipeline")]),
        ], indirect=True)
        return

    if is_blocked_module(mod):
        org_units = [
            # tests for blocked services are expected to fail with SCP in effect
            pytest.param(TEST_ORG, marks=[pytest.mark.raises_access_denied]),
        ]

        if not module_requires_scp(mod):
            # tests for (most) blocked services are expected to pass in setup
            # org. the exception is for tests that must *ONLY* be executed with
            # the SCP in effect (such as Shield, which would have a significant
            # monetary impact if the test passed because it was executed with
            # no SCP in effect).
            org_units.insert(0, pytest.param(SETUP_ORG))
    elif test_with_and_without_scp(metafunc):
        org_units.insert(0, pytest.param(SETUP_ORG))

    metafunc.parametrize('in_correct_org', org_units, indirect=True)


def test_with_and_without_scp(metafunc):
    """Determine if the specified test function has the
    "test_with_and_without_scp" marker, indicating that it must be tested in
    both situations without the "raises_access_denied" marker.

    :returns:
        A boolean.

    """

    return "test_with_and_without_scp" in {
        mark.name
        for mark in metafunc.definition.iter_markers()
    }


def pytest_collection_modifyitems(items):
    """Sort the tests so all tests in the ``SETUP_ORG`` happen first."""

    in_setup_org = []
    in_test_org = []

    for item in items:
        keys = item.keywords.keys()
        for key in keys:
            if SETUP_ORG in key:
                in_setup_org.append(item)
                break
        else:
            in_test_org.append(item)

    items[:] = in_setup_org + in_test_org


def is_blocked(*services):
    """Determine whether any of the specified services are blocked.

    :param str services:
        One or more service names.

    :returns:
        A boolean.

    """

    return bool(set(services) & (BLOCKED_SERVICES | SCP_REQD_SERVICES))


def is_blocked_module(module=None):
    """Determine whether the specified Python module is blocked.

    :param module: (optional)
        A Python module object. If omitted, the module is determined to be that
        which invoked this function.

    :returns:
        A boolean.

    """

    if module is None:
        # inspect the calling scope
        frame = inspect.stack()[1].frame
        module = inspect.getmodule(frame)

    services = get_service_names_from_mod(module)
    return is_blocked(*services)


@pytest.fixture(scope='session')
def is_blocked_service():
    """Return a function to determine whether the current test module is
    testing a blocked service.

    """

    return is_blocked_module


def requires_scp(*services):
    """Determines whether any of the specified services require a Service
    Control Policy to be in effect before testing.

    :param str services:
        One or more service names.

    :returns:
        A boolean.

    """

    return bool(set(services) & SCP_REQD_SERVICES)


def module_requires_scp(module):
    """Determine whether the specified Python module requires a Service Control
    Policy to be in effect.

    :param module:
        A Python module object.

    :returns:
        A boolean.

    """

    services = get_service_names_from_mod(module)
    return requires_scp(*services)


def get_service_names_from_mod(mod):
    """Determine which AWS service or services a module will test.

    Each test module may specify a service name or list of service names using
    an ``AWS_SERVICE`` global variable. For example:

    .. code-block:: python

        # specify a single AWS service
        AWS_SERVICE = 'batch'

        # specify multiple AWS services
        AWS_SERVICE = ['batch', 'ecs']

    If this global variable is not defined, the service name is determined
    using the test module's filename. For example:

    * ``test_ebs.py`` is assumed to test AWS EBS and will result in ``ebs``
      being the service returned by this function.
    * ``test_iam_users.py`` is assumed to test AWS IAM, but the result from
      this function will be ``['iam', 'iam_users']``.

    :param module mod:
        A Python module containing test functions.

    :returns:
        A set.

    """

    services = getattr(mod, 'AWS_SERVICE', None)
    if services is None:
        test_name = mod.__name__.replace('test_', '')
        services = [test_name]
        if '_' in test_name:
            services.append(test_name.split('_')[0])

    # make sure we always return a list
    if isinstance(services, str):
        services = [services]

    return set(services)


@pytest.fixture(scope='module')
def wait_for_scp(request, in_correct_org, global_session):
    """Return a function that checks to see if the Service Control Policy is in
    effect when we're in the ``TEST_ORG`` by waiting for an AccessDenied
    exception to appear when calling an API function that should be blocked by
    the SCP.

    This is an effort to handle the lag when switching between OUs.

    """

    def wrapped(func, args=None, kwargs=None, needle='AccessDenied', _raise=True,
                delay=30, max_attempts=24, mod=None):

        if mod is None:
            # inspect the calling scope
            frame = inspect.stack()[1].frame
            mod = inspect.getmodule(frame)

        if not is_blocked_module(mod):
            print('Module {} does not appear to be testing any blocked services'.format(mod.__name__))
            return True

        if args is None:
            args = tuple()

        if kwargs is None:
            kwargs = dict()

        name = func.__name__
        active = False

        if in_correct_org is global_session:
            print('Checking SCP for {}'.format(name))
            for attempt in range(max_attempts):
                try:
                    func(*args, **kwargs)
                except ClientError as ex:
                    print(ex)
                    if needle in str(ex):
                        print('SCP appears to be in effect now')
                        active = True
                        break
                else:
                    print('Waiting for SCP to be in effect for {} (attempt #{})'.format(name, attempt + 1))
                    time.sleep(delay)
        else:
            active = True

        if _raise:
            assert active, 'Service Control Policy not in effect for {} as expected'.format(name)

        return active

    return wrapped


@pytest.fixture(scope='function', autouse=True)
def scp(request, wait_for_scp):
    """Determine whether the test requires SCP verification and, if so, whether
    the SCP appears to be in effect.

    This fixture gets applied automatically for every test function unless a
    module overrides it.

    To configure this fixture for a particular module or service, simply create
    a new marker at the module level with the API function to invoke:

    .. code-block:: python

        pytestmark = [
            pytest.mark.scp_check('name_of_api_call'),
        ]

    An attempt will be made to automatically use the correct AWS client fixture
    for the service being tested to invoke the specified API function. If you
    would need to explicitly specify the client fixture name (such as for
    Lambda, whose client fixture is ``_lambda`` instead of ``lambda``), you may
    do so as such:

    .. code-block:: python

        pytestmark = [
            pytest.mark.scp_check('name_of_api_call', client_fixture='_lambda'),
        ]

    If the API function requires parameters, they may be specified using the
    ``args`` and ``kwargs`` arguments:

    .. code-block:: python

        pytestmark = [
            pytest.mark.scp_check('name_of_api_call', args=(1, 2, 3), kwargs={'a': 'b'}),
        ]

    If your SCP check is more involved than a single API function call, simply
    override this fixture as such:

    .. code-block:: python

        from aws_test_functions import is_blocked_module


        @pytest.mark.fixture(scope='function', autouse=is_blocked_module())
        def scp(client_fixture, wait_for_scp):
            wait_for_scp(client_fixture.scp_check_api_function)

    """

    if not is_blocked_module(request.module):
        # print('No need for implicit SCP check: service is not blocked')
        return

    check = getattr(request.module, '_implicit_scp_checker', None)
    if check is None:
        check = _prepare_implied_scp_checker(request, wait_for_scp)
        request.module._implicit_scp_checker = check

    check()


def _prepare_implied_scp_checker(request, wait_for_scp):
    """Create a new function that will try to determine whether it is safe to
    proceed with tests for a particular service based on whether the SCP for
    that service is in effect.

    :param pytest.FixtureRequest request:
        Information about the requesting test function.
    :param callable wait_for_scp:
        The output of the ``wait_for_scp`` fixture.

    :returns:
        A callable which takes no parameters.

    """

    cfg = _get_implied_scp_checker_config(request.module)
    if cfg is None:
        # no configuration found for implicit SCP check; return bool, which is
        # effectively a no-op
        return bool

    client_fixture = cfg.kwargs.pop('client_fixture', None)
    if client_fixture is None:
        client_fixture = list(get_service_names_from_mod(request.module))[0]

    def _make(client, api_function, *args, **kwargs):
        msg = 'Implicit SCP check using wait_for_scp({})'.format(
            ', '.join([
                '{}.{}'.format(client_fixture, api_function)
            ] + [
                repr(a) for a in args
            ] + [
                '{}={}'.format(k, repr(v)) for k, v in kwargs.items()
            ])
        )

        def _check():
            print(msg)
            wait_for_scp(getattr(client, api_function), mod=request.module, *args, **kwargs)

        return _check

    c = request.getfixturevalue(client_fixture)
    return _make(c, *cfg.args, **cfg.kwargs)


def _get_implied_scp_checker_config(module):
    """Try to locate the configuration for the implied SCP checker fixture.

    :param module module:
        A Python module.

    :returns:
        A pytest MarkInfo object or None.

    """

    cfg = None

    try:
        mod_marks = module.pytestmark
        if not isinstance(mod_marks, Iterable):
            mod_marks = [mod_marks]

        for mark in mod_marks:
            if mark.name == 'scp_check':
                cfg = mark
                break
    except AttributeError:
        # no configuration for implicit SCP check available without ``pytestmark``
        pass

    return cfg


pytest.is_blocked = is_blocked
pytest.is_blocked_module = is_blocked_module
