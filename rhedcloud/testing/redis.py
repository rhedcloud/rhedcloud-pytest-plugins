"""
=============
Redis Helpers
=============

"""

from redis import StrictRedis

from . import env


def get_redis():
    """Create and return a client connection to a Redis server.

    The target Redis server can be configured using the following environment
    variables:

    * :envvar:`REDIS_HOST`: hostname or IP of the Redis server.
    * :envvar:`REDIS_PORT`: port on which Redis is accepting clients.
    * :envvar:`REDIS_DB`: Redis database number to use. Default is ``0``.
    * :envvar:`REDIS_PASSWORD`: password required to access Redis server.

    If no hostname or port are specified, no Redis connection will be created.

    """

    if env.REDIS_HOST and env.REDIS_PORT:
        return StrictRedis(
            host=env.REDIS_HOST,
            port=env.REDIS_PORT,
            db=env.REDIS_DB,
            password=env.REDIS_PASSWORD,
        )
