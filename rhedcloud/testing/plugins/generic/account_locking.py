"""
===============
Account Locking
===============

This is a generic ``pytest`` plugin to support exclusive access to a testing
account for the duration of the ``pytest`` session.

"""

from getpass import getuser
import os
import threading
import time

from redis import StrictRedis
import pytest

from rhedcloud.testing.redis import get_redis

REDIS_KEY_PREFIX = os.getenv("REDIS_KEY_PREFIX", "lock")
REDIS_LOCK_SECONDS = max(int(os.getenv("REDIS_LOCK_SECONDS", 60)), 10)
REDIS_LOCK_ENABLED = bool(int(os.getenv("REDIS_LOCK_ENABLED", 1)))


@pytest.fixture(scope='session', autouse=REDIS_LOCK_ENABLED)
def auto_sync_tests(account_locking_key):
    """This session-wide fixture helps synchronize testing so there is less of
    a chance for errors caused by concurrent tests.

    During the course of a normal execution of ``pytest``, this fixture should
    be pretty meaningless. However, if two testers attempt to run tests at the
    same time on their local machines, there could be a conflict when one
    tester's instance of ``pytest`` moves the AWS account out of an OU before
    the second tester's instance of ``pytest`` is ready for that.

    This fixture is a way to avoid that situation by acquiring a lock on the
    AWS account for the duration of the ``pytest`` execution. If the account is
    already locked by another user, this fixture will wait until the lock is
    released before running any tests.

    If the ``pytest`` instance that acquired the lock crashes or otherwise does
    not release the lock, the lock will automatically expire after a certain
    amount of time (controlled by :envvar:`REDIS_LOCK_SECONDS`).

    The following environment variables control the behavior:

    * :envvar:`REDIS_HOST`: hostname or IP of the Redis server.
    * :envvar:`REDIS_PORT`: port on which Redis is accepting clients.
    * :envvar:`REDIS_DB`: Redis database number to use. Default is ``0``.
    * :envvar:`REDIS_PASSWORD`: password required to access Redis server.
    * :envvar:`REDIS_KEY_PREFIX`: prefix for keys in Redis. Default is
      ``lock``.
    * :envvar:`REDIS_LOCK_SECONDS`: maximum number of seconds to hold the lock.
      Default is ``60``. Minimum lock time is ``10`` seconds.

    Both :envvar:`REDIS_HOST` and :envvar:`REDIS_PORT` must be defined in order
    for locking to take effect. These values should be the same for all who
    will be running tests for the same AWS account.

    """

    r = get_redis()
    if r is not None:
        key = ':'.join([REDIS_KEY_PREFIX, account_locking_key])
        value = os.getenv('GIT_AUTHOR_EMAIL', getuser())
        ev = threading.Event()
        renewer = threading.Thread(target=renew_lock, args=(ev, r, key))
        renewer.daemon = True

        try:
            print('Acquiring lock on "{}"'.format(account_locking_key))
            while r.set(key, value, ex=REDIS_LOCK_SECONDS, nx=True) != 1:
                owner = r.get(key)
                print('>> Lock already owned by {}'.format(owner.decode()))
                time.sleep(30)

            print('Lock acquired on "{}"'.format(account_locking_key))
            renewer.start()

            yield
        finally:
            if renewer.is_alive():
                print('Releasing lock on "{}"'.format(account_locking_key))
                ev.set()
                renewer.join(10)

                r.delete(key)
    else:
        print('locking disabled because REDIS_HOST and REDIS_PORT are not both set')
        yield


def renew_lock(ev: threading.Event, r: StrictRedis, key: str):
    """Periodically renew the lock on the AWS account.

    :param threading.Event ev:
        Event object used to signal when it's time to stop renewing the lock.
    :param redis.StrictRedis r:
        Existing Redis client.
    :param str key:
        Redis key whose TTL will be extended.

    """

    while not ev.is_set():
        r.expire(key, REDIS_LOCK_SECONDS)
        time.sleep(int(REDIS_LOCK_SECONDS * 0.75))
