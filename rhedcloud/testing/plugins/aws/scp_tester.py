"""
==============
AWS SCP Tester
==============

This plugin contains various fixtures that are shared across repos that test
AWS Service Control Policies.

"""

from contextlib import ExitStack
import os
import sys

import boto3
import pytest
import _pytest.main

from aws_test_functions import (
    TEST_PROFILE,
    aws_profile,
    debug,
    env,
    get_account_info,
    get_policy_arn,
    get_vpc_ids,
    new_user,
    orgs,
    sts,
)


pytest_plugins = [
    'rhedcloud.testing.plugins.aws.account_locking',
    'rhedcloud.testing.plugins.aws.accessdenied',
    # 'rhedcloud.testing.plugins.aws.org_shuffle',
    'rhedcloud.testing.plugins.aws.blocked_service',
    'rhedcloud.testing.plugins.aws.script_runner',
    'rhedcloud.testing.plugins.generic.documented_tests',
    'rhedcloud.testing.plugins.generic.dict_fixture',
    'rhedcloud.testing.plugins.generic.configurableskip',
]


# override exit code for when no tests are collected
_pytest.main.EXIT_NOTESTSCOLLECTED = 0

# create a RHEDcloudMaintenanceOperatorRole session in the target AWS account.
SESSION = sts.get_target_account_session(os.getenv("RHEDCLOUD_ACCOUNT_NAME"), "MAINT")

# the sts.get_target_account_session call forces the boto3.DEFAULT_SESSION to
# be initialized with a session that is authenticated in the master AWS
# account, so this line must come afterwards in order to be useful.
MASTER_SESSION = boto3.DEFAULT_SESSION

# attempt to override the default boto3 session to reference the session we
# just created for the maintenance role assumed above
boto3.DEFAULT_SESSION = SESSION
_creds = SESSION.get_credentials()
os.unsetenv('AWS_PROFILE')
os.environ['AWS_ACCESS_KEY_ID'] = _creds.access_key
os.environ['AWS_SECRET_ACCESS_KEY'] = _creds.secret_key
os.environ['AWS_SESSION_TOKEN'] = _creds.token

SESSION_STACK = ExitStack()
VPC_IDS = get_vpc_ids(env.RHEDCLOUD_VPC_TYPE, ec2=SESSION.resource('ec2'))


@pytest.fixture(scope='session')
def master_session():
    """Return a session for the user that is authenticated to the master
    account, scoped to the pytest session.

    """

    return MASTER_SESSION


@pytest.fixture(scope='session')
def maint_session():
    """Create a session for the test user that is scoped to the pytest session
    and assumes the RHEDcloudMaintenanceOperatorRole in the target account.

    """

    return SESSION


@pytest.fixture(scope='module', autouse=orgs.AUTOMOVE_ACCOUNT)
def in_correct_org(maint_session, global_session, request):
    """Fixture to automatically shuffle the AWS account between the
    ``SETUP_ORG`` and ``TEST_ORG`` for each test function, if necessary.

    Each repository should define its own ``setup_fixtures`` module-scoped
    fixture to group any other fixtures that need to be setup prior to
    switching to the ``TEST_ORG``.

    """

    yield request.param


@pytest.fixture(scope='session', autouse=orgs.AUTOMOVE_ACCOUNT)
def in_setup_org(request):
    return SESSION


@pytest.fixture(scope='session', params=VPC_IDS)
def vpc_id(request):
    """ID of an RHEDcloud VPC.

    This fixture overrides the ``vpc_id`` fixture that is provided by the
    ``aws_script_runner`` plugin. It is necessary to override because we need
    to get the VPCs from a different AWS account than the one used when
    ``pytest`` runs for this repository.

    :param pytest.request request:
        A pytest request object.

    :returns:
        A string.

    """

    print('Using VPC (type {}): {}'.format(env.RHEDCLOUD_VPC_TYPE, request.param))
    return request.param


@pytest.hookimpl(tryfirst=True)
def pytest_cmdline_preparse():
    """Check to see whether the CloudFormation stack appears to be valid.

    * 2018-04-03: The RHEDcloudAccountCloudTrailPolicy was merged into
      RHEDcloudAdministratorRole. Any stack that still has this policy should be
      rebuilt.

    """

    try:
        with aws_profile(TEST_PROFILE):
            iam = boto3.resource('iam')
            policy = iam.Policy(get_policy_arn('RHEDcloudAccountCloudTrailPolicy'))

            # check to see whether the policy exists
            policy.load()
    except Exception as ex:
        if 'NoSuchEntity' not in str(ex):
            print('ERROR:', ex)
    else:
        print(
            '\n\n\tRHEDcloudAccountCloudTrailPolicy found. Please rebuild your '
            'stack or manually delete it.\n\n',
            file=sys.stderr,
        )
        sys.exit(1)


@pytest.fixture(scope='session')
def user_prefix():
    """Prefix to use when creating test users for this repository."""

    return 'test_standard_scp'


@pytest.fixture(scope='session')
def test_user_policies():
    """IAM Policies to attach to test users."""

    return (
        'rhedcloud/RHEDcloudAdministratorRolePolicy',
        'rhedcloud/rhedcloud-aws-vpc-type1-RHEDcloudManagementSubnetPolicy',
        'AdministratorAccess',
    )


@pytest.fixture(scope='session', autouse=True)
def user(maint_session, user_prefix, test_user_policies, session_stack):
    """Create a default test user"""

    u = session_stack.enter_context(new_user(
        user_prefix,
        *test_user_policies,
        stack=session_stack,
        iam=maint_session.resource('iam'),
    ))

    yield u


@pytest.fixture(scope='session', autouse=True)
def user_two(maint_session, user_prefix, test_user_policies, session_stack):
    """Create a secondary test user"""

    with new_user(user_prefix, *test_user_policies, stack=session_stack) as u:
        yield u


@pytest.fixture(scope='module')
def session(in_correct_org, global_session, maint_session):
    """Create a default session for the test user that is scoped to the pytest module."""

    return global_session


@pytest.fixture(scope='module', autouse=True)
def shared_vars(session):
    """Placeholder for test modules to track locally-shared values."""

    yield {}


@pytest.fixture(scope='session', autouse=True)
def session_stack():
    """Create a reusable context manager per test session.

    :yields:
        A contextlib.ExitStack.

    """

    with SESSION_STACK as s:
        yield s


@pytest.fixture(scope='module', autouse=True)
def stack():
    """Create a reusable context manager per test module.

    :yields:
        A contextlib.ExitStack.

    """

    with ExitStack() as s:
        yield s


@pytest.fixture(scope="module")
def assume_role(master_session):
    """Fixture to simplify switching between roles assumed for testing
    purposes.

    Usage:

    .. code-block:: python

        import pytest


        @pytest.fixture(scope="module")
        def ir_session(assume_role):
            return assume_role("role/rhedcloud/RHEDcloudSecurityIRRole")


        @pytest.fixture(scope="module")
        def ir_ec2(ir_session):
            return ir_session.client("ec2")


        def test_something_with_ir_role(ir_ec2):
            ...

    """

    def inner(assume_role, *args, **kwargs):
        """Create a new boto3 session for the specified role.

        :param str assume_role:
            Name of the IAM Role to assume in the target account.

        :returns:
            A Session object configured to use the AWS API within the context
            of the target AWS account as the specified role.

        """

        debug("Current identity: {}".format(get_account_info()["Arn"]))

        kwargs["assume_role"] = assume_role

        # default to using the STS API in the master AWS account
        if "sts" not in kwargs:
            kwargs["sts"] = master_session.client("sts")

        # default to using the Organizations API in the master AWS account
        if "organizations" not in kwargs:
            kwargs["organizations"] = master_session.client("organizations")

        # finally, assume the specified role in the target AWS account
        new_session = sts.get_target_account_session(
            os.getenv("RHEDCLOUD_ACCOUNT_NAME"), "TEST",
            *args, **kwargs
        )

        # provide a little debug output to confirm which role we've assumed in
        # the target account
        new_sts = new_session.client("sts")
        debug("New identity: {}".format(get_account_info(sts=new_sts)["Arn"]))

        return new_session

    return inner


@pytest.fixture(scope="session")
def master_account_number(master_session):
    """Retrieve the account number for the master account.

    :param boto3.session.Session master_session:
        A boto3 session for the master account.

    :returns:
        A string.

    """

    sts = master_session.client("sts")
    return get_account_info(sts=sts)["Account"]


@pytest.fixture(scope="session")
def target_account_number(maint_session):
    """Retrieve the account number for the target account.

    :param boto3.session.Session maint_session:
        A boto3 session for the target account.

    :returns:
        A string.

    """

    sts = maint_session.client("sts")
    return get_account_info(sts=sts)["Account"]


@pytest.fixture(scope="session")
def target_ou(master_session):
    """Retrieve information about the target account's Organizational Unit.

    :param boto3.session.Session master_session:
        A boto3 session for the master account, to examine the Service Control
        Policies attached to an Organizational Unit.

    :returns:
        A dictionary.

    """

    orgs_client = master_session.client("organizations")
    return orgs.lookup_ou(
        orgs.TEST_ORG,
        organizations=orgs_client,
    )


@pytest.fixture(scope="session", autouse=True)
def scp_name():
    """Return the name of the current pipeline's Service Control Policy."""

    return os.environ["SCP_NAME"]


@pytest.fixture(scope="session")
def is_scp_active(master_session, target_ou, scp_name):
    """Determine whether the Service Control Policy being tested is currently
    attached to the target account's organizational unit.

    :param boto3.session.Session master_session:
        A boto3 session for the master account, to examine the Service Control
        Policies attached to an Organizational Unit.
    :param dict target_ou:
        Information about the target account's Organizational Unit.
    :param str scp_name:
        Name of the Service Control Policy we need to search for.

    :returns:
        A boolean.

    """

    orgs = master_session.client("organizations")
    res = orgs.list_policies_for_target(
        TargetId=target_ou["Id"],
        Filter="SERVICE_CONTROL_POLICY",
    )

    attached = False
    for p in res["Policies"]:
        if scp_name in p["Description"]:
            attached = True
            break

    return attached
